package jenko.b_comix;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatStruct extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    String mBackGround;
    String mCharacter;
    String mMessage;
    String mDstPhoneNumber;
    ImageView cheracterImageView;
    ImageView backGroundImageView;
    GlobalVariables global;
    DatabaseReference mPagesDB;
    List<Map<String,Object>> pages_list;


    Button send_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_struct);
        global = new GlobalVariables();
        cheracterImageView = (ImageView) findViewById(R.id.CharecterImageView);
        backGroundImageView = (ImageView)findViewById(R.id.BackGroundViewImage);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
        mDatabase.addValueEventListener(chat_struct_listener);
        send_btn = (Button) findViewById(R.id.send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText output = (EditText) findViewById(R.id.msg_output);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("public_queues").child(global.getDstUserID());
                //DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy mm:HH");
                //Calendar cal = Calendar.getInstance();
                ChatMessage chatMsg = new ChatMessage(mCharacter,mBackGround,output.getText().toString(),global.getDstUserID(),mAuth.getUid(),"temp","date","time");
                mDatabase.push().setValue(chatMsg);
                DatabaseReference myDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid()).child("books").child(global.getDstUserID());
                myDatabase.push().setValue(chatMsg);
                startActivity(new Intent(ChatStruct.this, CarouselPreviewActivity.class));
                finish();
            }
        });
    }

    ValueEventListener chat_struct_listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            mCharacter = user.figure;
            mBackGround = global.getScreen();
            switch (mCharacter){
                case "grandpa_with_guitar":
                    cheracterImageView.setImageResource(R.drawable.grandpa_with_guitar);
                    break;
                case "grandpa_on_chair":
                    cheracterImageView.setImageResource(R.drawable.grandpa_on_chair);
                    break;
                case "man_with_radio":
                    cheracterImageView.setImageResource(R.drawable.man_with_radio);

                    break;
                case "man_with_paresuit":
                    cheracterImageView.setImageResource(R.drawable.man_with_paresuit);

                    break;
                case "red_dress_women":
                    cheracterImageView.setImageResource(R.drawable.red_dress_women);

                    break;
            }

            switch (mBackGround){
                case "dot_back":
                    backGroundImageView.setImageResource(R.drawable.dot_back);
                    break;
                case "room_back":
                    backGroundImageView.setImageResource(R.drawable.room_back);
                    break;
                case "gray_back":
                    backGroundImageView.setImageResource(R.drawable.gray_back);
                    break;
                case "sea_backs_quare":
                    backGroundImageView.setImageResource(R.drawable.sea_backs_quare);
                    break;
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //problema
        }
    };


    public Integer[] arrFactor(Integer[] arr){
        Integer[] newArr = new Integer[arr.length + 1];
        if(arr.length > 0){
            for (int i = 0; i < arr.length; i++)
            {
                newArr[i] = arr[i];
            }
        }
        newArr[newArr.length -1] = R.color.color_8;
        return newArr;
    }


}



