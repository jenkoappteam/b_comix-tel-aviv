package jenko.b_comix;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class ActivityAcceptFriendRequest extends AppCompatActivity {

    String mFriendID;
    String mFriendNickname;
    String mFriendFigure;
    DatabaseReference friendDB;
    ImageView mFigureImageView;
    ImageView mYesImageview;
    ImageView mNoImageview;
    TextView mTitle;
    String type;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_friend_request);
        mAuth = FirebaseAuth.getInstance();
        mFigureImageView = findViewById(R.id.figureImageView);
        mYesImageview = findViewById(R.id.yesBtn_imageView);
        mNoImageview = findViewById(R.id.noBtn_imageView);
        mTitle = (TextView) findViewById(R.id.headlineTextView);
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        mTitle.setText(title);
        type = intent.getStringExtra("type");
        if(type.equals("chat")) {
            mFriendID = intent.getStringExtra("id");
            mFriendNickname = intent.getStringExtra("nickname");


            friendDB = FirebaseDatabase.getInstance().getReference().child("users").child(mFriendID);
            friendDB.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Map<String, Object> temp_map = (Map<String, Object>) dataSnapshot.getValue();
                        mFriendFigure = (String) temp_map.get("figure");
                        switch (mFriendFigure) {
                            case "grandpa_with_guitar":
                                mFigureImageView.setImageResource(R.drawable.grandpa_with_guitar);
                                break;
                            case "grandpa_on_chair":
                                mFigureImageView.setImageResource(R.drawable.grandpa_on_chair);
                                break;
                            case "man_with_radio":
                                mFigureImageView.setImageResource(R.drawable.man_with_radio);

                                break;
                            case "man_with_paresuit":
                                mFigureImageView.setImageResource(R.drawable.man_with_paresuit);

                                break;
                            case "red_dress_women":
                                mFigureImageView.setImageResource(R.drawable.red_dress_women);

                                break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

            mYesImageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseReference friendRequestDB = FirebaseDatabase.getInstance().getReference().child("friends_request").child(mFriendID);
                    DatabaseReference acceptDB = friendRequestDB.push();
                    acceptDB.setValue("yes&&" + mAuth.getUid() + "&&" + "none");
                    DatabaseReference myDB = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
                    myDB.child("friends").push().setValue(mFriendID);
                    Intent intent1 = new Intent(ActivityAcceptFriendRequest.this, LaunchActivity.class);
                    startActivity(intent1);
                }
            });

            mNoImageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(ActivityAcceptFriendRequest.this, LaunchActivity.class);
                    startActivity(intent1);
                }
            });
        }
        else if(type.equals("group")){
            String bookID = intent.getStringExtra("id");
        }



    }
}
