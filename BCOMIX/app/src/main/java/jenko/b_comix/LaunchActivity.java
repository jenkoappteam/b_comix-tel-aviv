package jenko.b_comix;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.firebase.ui.auth.ui.idp.IDPBaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LaunchActivity extends AppCompatActivity {

    private ImageView options;
    private ImageView createNexComic;
    private ImageView inviteFriend;


    LinearLayout mBook_layout;
    List<String> mRecentChats;
    Map<String,Object> mAll_books_data;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    SlidingUpPanelLayout mSlidingUpPanel;
    ImageView mUpArrow;
    User mUser;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        mUpArrow = (ImageView)findViewById(R.id.up_arrow_image_view);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(mAuth.getUid());
        mDatabase.addValueEventListener(recent_chats_listener);

        inviteFriend = (ImageView)findViewById(R.id.invite_friends);
        inviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchActivity.this,InviteFriendActivity.class));
            }
        });

        mSlidingUpPanel = (SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        mSlidingUpPanel.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if(newState.name().equalsIgnoreCase("Collapsed")){

                    mUpArrow.setScaleY(1);

                }else if(newState.name().equalsIgnoreCase("Expanded")){

                    mUpArrow.setScaleY(-1);
                }

            }
        });

        options = (ImageView) findViewById(R.id.options_button);
        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchActivity.this, Options.class));
            }
        });

        createNexComic = (ImageView) findViewById(R.id.start_new_comic);
        createNexComic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchActivity.this, BodySelect.class));
            }
        });


        ImageView createNewGroup = (ImageView)findViewById(R.id.start_new_group);
        createNewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchActivity.this,GroupChatBuddySelect.class));
            }
        });

        this.mBook_layout = (LinearLayout)findViewById(R.id.BooksLinearlayout);

        if (mAuth.getCurrentUser().getProviderData() == null)
        {
            startActivity(new Intent(LaunchActivity.this, UserInfo.class));
        }
    }


    ValueEventListener recent_chats_listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            //mUser = dataSnapshot.getValue(User.class);
            /*
            Map<String,Object> all_books = mUser.all_books;
            List<String> recent_chats = mUser.recent_chats;
            if(recent_chats!=null && !recent_chats.isEmpty()) {
                for (String recent : recent_chats) {
                    String[] parts = recent.split("###");
                    String friend_nickname = parts[0];
                    String book_number = parts[1];

                    String background;
                    String book_name;
                    String creation_date;
                    String friend_figure;
                    String my_figure;

                    Map<String, Object> friend_book = (HashMap<String, Object>) all_books.get(friend_nickname);
                    Map<String, Object> book = (HashMap<String, Object>) friend_book.get(book_number);
                    Map<String, Object> book_details = (HashMap<String, Object>) book.get("details");

                    background = (String) book_details.get("book_background");
                    book_name = (String) book_details.get("book_name");
                    creation_date = (String) book_details.get("creation_date");
                    friend_figure = (String) book_details.get("friend_figure");
                    my_figure = (String) book_details.get("my_figure");

                    BookFragment fragment = BookFragment.newInstance(background, book_name, my_figure, friend_figure, false);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.BooksLinearlayout, fragment, "myTag")
                            .commit();

                }
            }*/
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //problema
        }
    };
}
