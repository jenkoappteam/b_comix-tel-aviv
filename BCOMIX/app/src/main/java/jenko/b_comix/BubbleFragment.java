package jenko.b_comix;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.ContiguousSet;


public class BubbleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "name";
    private static final String ARG_PARAM2 = "side";
    private static final String ARG_PARAM3 = "userID";

    // TODO: Rename and change types of parameters
    private String name;
    private boolean side;
    String mUserID;
    ImageView mBubbleiV;
    TextView mNameTV;
    ImageView mCheckImageView;
    View mMyView;
    OnSelectClickListener mSelectClickListener;


    public BubbleFragment() {
        // Required empty public constructor
    }
    public static BubbleFragment newInstance(String name, boolean side, String userID) {
        BubbleFragment fragment = new BubbleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, name);
        args.putBoolean(ARG_PARAM2, side);
        args.putString(ARG_PARAM3,userID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(ARG_PARAM1);
            side = getArguments().getBoolean(ARG_PARAM2);
            mUserID = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_bubble, container, false);
        mMyView = view;
        ConstraintLayout layout = (ConstraintLayout)view.findViewById(R.id.frameLayout);
        ImageView bubbleIV = (ImageView)view.findViewById(R.id.bubbleImageView);
        TextView nameTV = (TextView)view.findViewById(R.id.nameTextView);
        ImageView selectBG = (ImageView)view.findViewById(R.id.selectBackground);
        mCheckImageView = selectBG;
        nameTV.setText(name);
        if(side){//left
            //bubbleIV.setScaleX(-1);
            ConstraintSet constraintSet =new ConstraintSet();
            constraintSet.clone(layout);
            selectBG.getLayoutParams().width = 250;

            constraintSet.connect(selectBG.getId(),ConstraintSet.START,layout.getId(),ConstraintSet.START);
            constraintSet.connect(bubbleIV.getId(),ConstraintSet.START,selectBG.getId(),ConstraintSet.START);
            constraintSet.connect(nameTV.getId(),ConstraintSet.START,layout.getId(),ConstraintSet.START);
            //constraintSet.connect(nameTV.getId(),ConstraintSet.BOTTOM,bubbleIV.getId(),ConstraintSet.BOTTOM);
            //constraintSet.connect(nameTV.getId(),ConstraintSet.TOP,bubbleIV.getId(),ConstraintSet.TOP);


            nameTV.setLeft(60);


            //constraintSet.connect(selectBG.getId(),ConstraintSet.END,bubbleIV.getId(),ConstraintSet.END);
            //nameTV.setPadding(80,nameTV.getCompoundPaddingTop(),nameTV.getCompoundPaddingRight(),nameTV.getCompoundPaddingBottom());
            constraintSet.applyTo(layout);
        }
        else{//right
            bubbleIV.setScaleX(1);
            ConstraintSet constraintSet =new ConstraintSet();
            constraintSet.clone(layout);
            selectBG.getLayoutParams().width = 240;
            constraintSet.connect(selectBG.getId(),ConstraintSet.END,layout.getId(),ConstraintSet.END);
            constraintSet.connect(bubbleIV.getId(),ConstraintSet.START,selectBG.getId(),ConstraintSet.START);
            constraintSet.connect(nameTV.getId(), ConstraintSet.START,bubbleIV.getId(),ConstraintSet.START);

            ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(25, 0, 0, 0);
            nameTV.setLayoutParams(lp);
            //constraintSet.connect(nameTV.getId(),ConstraintSet.START,bubbleIV.getId(),ConstraintSet.START,50);
            //constraintSet.connect(nameTV.getId(),ConstraintSet.END,bubbleIV.getId(),ConstraintSet.END);
            //constraintSet.connect(selectBG.getId(),ConstraintSet.END,bubbleIV.getId(),ConstraintSet.END);
            //nameTV.offsetLeftAndRight(80);
            //nameTV.setLeft(bubbleIV.getLeft()+80);
            //nameTV.setPadding(nameTV.getCompoundPaddingLeft()+50,nameTV.getCompoundPaddingTop(),nameTV.getCompoundPaddingRight(),nameTV.getCompoundPaddingBottom());
            constraintSet.applyTo(layout);
        }
        this.mNameTV = nameTV;
        this.mBubbleiV =bubbleIV;
        selectBG.setVisibility(View.INVISIBLE);
        this.mBubbleiV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView mySelect = (ImageView)mMyView.findViewById(R.id.selectBackground);
                mySelect.setVisibility(View.VISIBLE);
                mSelectClickListener.onSelectClickListener(mNameTV.getText().toString(),mUserID);

            }
        });
        return view;
    }

   public void hidecheck(){
        ImageView mySelect = (ImageView)mMyView.findViewById(R.id.selectBackground);
        mySelect.setVisibility(View.INVISIBLE);
   }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = (Activity)context;
        try{
            mSelectClickListener = (OnSelectClickListener)activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"nust override onSelectClick...");
        }
    }

    public interface OnSelectClickListener{
        public void onSelectClickListener(String nickname, String userID);
    }
    public int getID(){
        return BubbleFragment.this.getId();
    }
}
