package jenko.b_comix;

public class Character {
    private int imageSource;

    public Character (int imageSource) {
        this.imageSource = imageSource;
    }

    public int getImageSource() {
        return imageSource;
    }
}
